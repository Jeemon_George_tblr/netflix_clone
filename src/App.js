import NavBar from './Components/NavBar/NavBar';
import Baner from './Components/Banner/Baner';
import RowPost from './Components/RowPost/RowPost';
import './App.css'
import {action,originals} from './urls'
function App() {
  return (
    <div className='App'>

      <NavBar/>
      <Baner/>
      <RowPost url={originals} title='Netflix Clone Orginals'/>
      <RowPost url={action} title='Action' isSmall/>


    </div>
  );
}

export default App;
